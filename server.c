#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
 
#define PORTNUM 5566
#define RESULT_TIME 1002
#define RESULT_A 1003
 
int main(int argc, char *argv[])
{
    int time = 3002;
    int a = 3003;
    int polecenie;
    int czas = 123456789;
 
    struct sockaddr_in client; /* socket info about the machine connecting to us */
    struct sockaddr_in serv; /* socket info about our server */
    int mysocket;            /* socket used to listen for incoming connections */
    socklen_t socksize = sizeof(struct sockaddr_in);
 
    memset(&serv, 0, sizeof(serv));           /* zero the struct before filling the fields */
    serv.sin_family = AF_INET;                /* set the type of connection to TCP/IP */
    serv.sin_addr.s_addr = htonl(INADDR_ANY); /* set our address to any interface */
    serv.sin_port = htons(PORTNUM);           /* set the server port number */    
 
    mysocket = socket(AF_INET, SOCK_STREAM, 0);
 
    /* bind serv information to mysocket */
    bind(mysocket, (struct sockaddr *)&serv, sizeof(struct sockaddr));
 
    /* start listening, allowing a queue of up to 1 pending connection */
    listen(mysocket, 1);
    int consocket = accept(mysocket, (struct sockaddr *)&client, &socksize);
 
    while(consocket)
    {
        //send(consocket, msg, strlen(msg), 0); 
   	recv(consocket , &polecenie , sizeof(int) , 0);
   	printf ("Wiadomosc od klienta: %d\n", polecenie);
        if (polecenie == RESULT_TIME)
	{
	    send(mysocket , &time , sizeof(int),0);
            recv(consocket , &polecenie , sizeof(int) , 0);
            if (polecenie == 3002)
            {
             send(mysocket , &czas , sizeof(int),0);
 	    }
        }

        close(consocket);
        consocket = accept(mysocket, (struct sockaddr *)&client, &socksize);
    }
 
    close(mysocket);
    return EXIT_SUCCESS;
}